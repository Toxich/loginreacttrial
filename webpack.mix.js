const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .react()
    .postCss('resources/css/app.css', 'public/css', [require('tailwindcss'), require('autoprefixer')])
    .alias({
        '@': 'resources/js',
    })
    .sass('resources/styles/style.scss', 'public/styles/style.css')
    .js('resources/scripts/app.js', 'public/js')
    .scripts([
      'resources/scripts/lib/jquery.simplePagination.js',
      'resources/scripts/lib/_gettext.js',
      'resources/scripts/lib/_mustache.js',
      'resources/scripts/lib/jquery.autocomplete.js',
  
      'resources/scripts/pages/statics/faq.js',
  
      'resources/scripts/shared/partials/header.js',
      'resources/scripts/shared/partials/detecting_scroll_direction.js',
      'resources/scripts/shared/common.js',
  
      'resources/scripts/elasticsearch_for_grid.js',
      'resources/scripts/pages/search/search_elasticsearch.js',
      ], 'public/js/scripts/app.js')
    .browserSync({
      proxy: 'localhost:8002',
    });

if (mix.inProduction()) {
    mix.version();
}
